"""
deck module
"""
class Deck:
    """
    Deck class
    """
    def __init__(self):
        self.cards = []

    def shuffle(self):
        """
        shuffle deck cards
        :return:
        """
        return self.cards

    def __str__(self):
        return f'{len(self.cards)} cards'
