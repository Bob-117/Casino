"""
card module
"""

class Card:
    """
    Card class
    """
    def __init__(self, value, face):
        self.value = value
        self.face = face
        self.power = self.value

    def __gt__(self, other):
        return self.power > other.power

    def __lt__(self, other):
        return self.power < other.power

    def __eq__(self, other):
        # print(f'{self.value} - {other.value}')
        # print(other.value)
        # if self.value == other.value and self.face == self.face:
            # print(f'{self} - {other}')
            # print(type(other.face))
        return (self.value == other.value) and (self.face == self.face)

    def __str__(self):
        """
        print
        :return:
        """
        return f'{self.value}-{self.face}'

    def __repr__(self):
        """
        print([self])
        :return:
        """
        return f'{self.value}-{self.face}'

    def update_power(self, new_power):
        """
        set card power depending on current game rules
        :param new_power:
        :return:
        """
        self.power = new_power

    def im_in_hand(self, hand):
        """
        a card can tell if she's in a player hand
        :param hand:
        :return:
        """
        # print('--------im in hand--------')
        # list(map(lambda player: len(player.hand) > 0, self.players))
        # print('----------------')
        # output = False
        # for card in hand:
        #     # print(f'-{self} - {card}')
        #     if self == card:
        #         print(f'-{self} - {card}')
        # a = next(x for x in hand if x.face == 'R')

        # print('---------------------')
        # a =
        # print(list(a))
        # # print(self, hand, self in hand)
        # print('---------------------')
        # return self in hand
        return len(
            list(
                filter(
                    lambda card_in_hand:
                    card_in_hand.face == self.face and card_in_hand.value == self.value,
                    hand)
            )
        ) == 1
