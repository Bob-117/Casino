"""
Round module
handle the game from first card played to all players -1 can play (fold/empty hand)
"""
from src.utils import Const


class Round:
    """
    Round class
    handle the game from first card played to all players -1 can play (fold/empty hand)
    """

    def __init__(self, players, options, last_round=None):

        # init, play, end(swap card, count points)
        self.state = 'init' if not last_round else 'play'

        self.players = players
        self.options = options
        self.last_round = last_round

        self.first_player = None
        self.current_cards = []
        self.nb_card = 0

        self.winners = []

    def run(self):
        """
        run the round
        :return:
        """
        # if init => select first player else last winner starts
        print('-----')
        print(
            f'last : {self.last_round.state if self.last_round else None}, current : {self.state}, options : {self.options}')
        print('-----')

        self.select_first_player()
        current_player = self.first_player

        self.user_prompt(current_player)
        self.cheat_enable(current_player)
        self.nb_card = current_player.init_round(available_nb_card=Const.AVAILABLE_NB_CARD)

        current_card = current_player.pick_a_card(self.nb_card)
        self.current_cards.append(current_card)

        for _ in range(2):  # while at least 2 ppl can play
            # while self.what_player_can_play_in_players():
            current_player = self.players[(self.players.index(current_player) + 1) % len(self.players)]

            self.user_prompt(current_player)
            self.cheat_enable(current_player)

            current_card = current_player.pick_a_card(self.nb_card)
            if current_player.hand == 0:
                self.winners.append(current_player)

            self.current_cards.append(current_card)

        # ##
        # set winners
        return self

    def card_in_hands(self):
        """
        at least 2 players have 1 card in hand
        :return:
        """
        # print('--------im in hand--------')
        # list(map(lambda player: len(player.hand) > 0, self.players))
        # print('----------------')
        return list(map(lambda player: len(player.hand) > 0, self.players))

    def what_player_can_play_in_players(self):
        """
        at least 2 players have 1 playable card set (1 to 4 cards)
        and better than top card in hand (i.e. not fold)
        :return:
        """
        top_card = self.current_cards[-1]
        # print('--------im in hand--------')
        # list(map(lambda player: len(player.hand) > 0, self.players))
        # print('----------------')

        # a = list(map(lambda player: len(player.hand) > 0, self.players))
        # print(a)
        # b = list(map(lambda player: player.at_least_one_card_to_play(self.current_cards[-1]), self.players))
        # print(b)
        # c = list(map(lambda player: any(card in player.hand and card > card for card in top_card), self.players))
        players_status_can_play = list(
            map(
                lambda player:
                len(player.hand) > 0
                and not player.fold
                and any(card > top_card for card in player.hand),
                self.players
            )
        )
        print(players_status_can_play)
        # d = list(map(lambda player: any(card > top_card for card in player.hand), self.players))
        # print(d)
        return players_status_can_play

    def select_first_player(self):
        """
        Select first player for current round and set it to self.first_player
        if round state == init, first player has special card
        if round state == play, first player is the last round winner
        :return:
        """
        if self.state == 'init':
            print('INIT')
            for player in self.players:
                if player.first_player_special_card(self.options['special_card']):
                    print(f'{player.name} will start, hes has {self.options["special_card"]}')
                    self.first_player = player
        elif self.state == 'play':
            try:
                self.first_player = self.last_round.winners[0]
            except IndexError:
                print('oopsi')
                self.first_player = self.players[0]
            print(f'{self.first_player.name} will start, hes won last round')

    def cheat_enable(self, player):
        """
        :^)
        """
        if player.name.lower() == 'erwann':
            for _player in self.players:
                print(f'{_player} - {_player.hand}')

    def user_prompt(self, player):
        print(f'{player} - {player.hand}')
        print(f'{player.sort_cards_combo()}')
        print(f'Table :{self.current_cards}')
    # def run_old(self):
    #     print(f'start round X with {self.players} & options : {self.options}')
    #     a = input("suite")
    #     if not self.first_player:
    #         for player in self.players:
    #             if player.first_player(self.options['special_card']):
    #                 print(f'{player.name} will start, hes has {self.options["special_card"]}')
    #                 self.first_player = player
    #     current_player = self.first_player
    #     current_card = current_player.pick_a_card()
    #     self.current_cards.append(current_card)
    #     current_player.hand.remove(current_card)
    #     print(self.current_cards)
    #     print(current_player.hand)
    #
    #     while len(current_player.hand) > 5:  # while fold != [0, 1, 1]
    #         current_player = self.players[(self.players.index(current_player) + 1) % len(self.players)]
    #         current_card = current_player.pick_a_card()
    #         self.current_cards.append(current_card)
    #         current_player.hand.remove(current_card)
    #         print(self.current_cards)
    #         print(current_player.hand)
    #
    #     test = input('exit')
    #     if test == 'exit':
    #         exit()
