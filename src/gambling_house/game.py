"""
game module
"""
from random import shuffle

from src.cards import Card
from src.gambling_house.round import Round


class Game:
    """
    class Game
    handle players, deck & several rounds (until the win)
    """

    def __init__(self, rules):
        """
        class Game
        handle players, deck & several rounds (until the win)
        """
        self.players = []
        self.deck = []
        self.nb_cards = 0
        self.used_cards = []
        self.unused_cards = []
        self.rules = rules  # first card...
        self.currently_playing = False  # TODO state init, playing, result

    def while_playing(self):
        """
        Main game loop, we loop on several Round() instance until someone end the game
        :return:
        """
        # print('while player has 1 card')
        ######################
        # current_round = Round(
        #     players=self.players,
        #     options={'special_card': Card('Q', '♡'),
        #              'state': 'init/play/end'},
        #     last_round='init')
        # current_round.run()
        ######################
        _round = Round(
            self.players,
            options={'special_card': Card('Q', '♡'),
                     'state': 'init/play/end'}
        )
        _round.run()
        for i in range(2):  # while len(round.winners) != len(players) - 1
            # while len(self.folded_players) != len(self.players) - 1
            current_options = {'special_card': Card('Q', '♡'), 'state': 'init/play/end'}
            _round = Round(self.players, last_round=_round, options=current_options)
            _round.run()
            # if i == 9:
            #     game_round.state = 'end'

        ######################
        # print("WHILE PLAYING")
        # exit() if input() == 'exit' else input()

    def card_in_hands(self):
        return list(map(lambda player: len(player.hand) > 0, self.players))

    # def register_player(self, player: Player):
    def register_player(self, player):
        """
        Register one new player
        :param player: Player
        :return:
        """
        print(f'register {player.name}')
        self.players.append(player)

    # def register_players(self, players: [Player]):
    def register_players(self, players):
        """
        Register a new players list
        :param players: [Player]
        :return:
        """
        for player in players:
            self.register_player(player)

    def deal_cards(self):
        """
        split current deck in equal parts between all players
        keep unused_cards
        :return:
        """
        print('deal card game')

        unused_cards = len(self.deck) % len(self.players)
        # used_cards = len(self.deck) // len(self.players)
        # print(f'players : {len(self.players)}, '
        #       f'cards : {self.nb_cards} '
        #       f'every player get {used_cards}, stay  {unused_cards}')
        print(f'B4 suffle {len(self.deck)} == {self.deck}')
        shuffle(self.deck)
        # print(f'After suffle {len(self.cards)} == {self.cards}')

        for _ in range(unused_cards):
            self.unused_cards.append(self.deck.pop())
        self.used_cards = self.deck
        # print(f'After plop {len(self.cards)} == {self.cards}')
        hands = [self.deck[i::len(self.players)] for i in range(0, len(self.players))]
        for i, current_hand in enumerate(hands):
            current_player = self.players[i]
            current_player.hand = current_hand
            print(f'{current_player} - {len(current_hand)} == {current_hand}')
            # print(current_player.first_player_special_card(Card('Q', '♡')))
            # current_player.hand(current_hand)

    def create_cards(self):
        """
        create a standart 52 cards deck
        :return:
        """
        print('create card game')
        faces = ['♡', '♣', '♢', '♤']
        cards = []
        for i in range(13):
            for face in faces:
                card = Card(i + 1, face)
                cards.append(card)
        self.deck = cards
        self.nb_cards = len(self.deck)
        print(self.deck)

    def generate_cards(self):
        """
        create cards deck with game rules
        :return:
        """
        # faces = ['♡', '♣', '♢', '♤']
        # value = [3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", 1, 2]

        # self.deck = [
        #     Card(val, face).set_power(power)
        #     for val, power
        #     in enumerate(value) for face in faces
        # ]
        print('generate card game')

        faces = ['♡', '♣', '♢', '♤']
        values = [3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", 1, 2]
        default_values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K"]
        test_values = [4, 5, 'K', 1, 2]
        test_faces = ['♡', '♣', '♤']

        # tarot_values = ["fool", 21]

        def value_to_power(card):
            if not self.rules:
                current_power = default_values.index(card.value) + 1
                card.update_power(new_power=current_power)
            elif self.rules['mode'] == 'trouduc':
                current_power = values.index(card.value) + 1
                card.update_power(new_power=current_power)
            elif self.rules['mode'] == 'test':
                current_power = test_values.index(card.value) + 1
                card.update_power(new_power=current_power)
            return card
            #
            # print(card)
            # return card

        if self.rules['mode'] == 'test':
            values = test_values
            faces = test_faces

        self.deck = list(
            map(
                value_to_power,
                [(Card(value, face)) for value in values for face in faces]
            )
        )

        self.nb_cards = len(self.deck)
        # print(list(self.deck))
        return self.deck

    # def start(self):
    #     self.create_cards()
    #     a = self.generate_cards()
    #     print(a)
    #     # print(card)
    #     self.deck = a
    #     self.nb_cards = len(self.deck)
    #     print(self.deck)

    def start(self):
        """
        start the game
        :return:
        """
        print("START")
        print(self.players)
        # while len(self.players) < 3:
        #     self.register_player(AIPlayer())
        self.generate_cards()
        self.deal_cards()
        self.while_playing()
