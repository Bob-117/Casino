"""
Casino module, handle Game, Player, Round, cards
"""
import logging

from src.service import GameService
from src.user_interface import CLIUserInterface, TKUserInterface
from src.utils import Const


class Casino:
    """
    Casino class, handle Game, Player, Round, cards
    """

    def __init__(self, mode=None, lang=None):
        self.__log = self.init_logger()
        self.language = lang
        # self.language_file = None
        self.name = 'GIBSON CASINO'
        self.mode = mode if mode in Const.MODE_LIST else 'cli'
        self.evaluation = self.mode == Const.MODE_EVALUATION
        self.user_interface = None

    def init_logger(self):
        """
        init logger
        :return:
        """
        logger = logging.getLogger(self.__class__.__name__)
        log_format = '%(asctime)s %(levelname)-8s %(name)-15s %(message)s'
        log_date_format = '%Y-%m-%d %H:%M:%S'
        logging.basicConfig(format=log_format, datefmt=log_date_format, level='DEBUG')  # TODO env
        return logger

    # def open(self):
    #     print(f'RUNNING CASINO MODE {self.mode}')
    #
    #     run_options = {
    #         'cli': self.open_cli,
    #         'tk': self.open_tk,
    #         1: 'aaaaaaaaaa'
    #     }
    #
    #     mode = self.mode
    #     print(mode)
    #     print(run_options.__getitem__(self.mode))
    #
    #     # # TODO User.login() / User().register
    #     # action = run_options.get(self.mode, default='cli')
    #     # action()

    def open(self):
        """
        Casino is opening with ui mode as env var, creating a game and starting it
        :return:
        """
        # self.init_lang()
        # print(f'RUNNING CASINO MODE {self.mode}')
        #
        # actions = {
        #     'cli': self.open_cli,
        #     'tk': self.open_tk
        # }
        # current_mode = self.mode
        # print(current_mode)
        # action = actions.get(current_mode, self.debug)
        # action()
        print(f'{self.language.WELCOME} {self.name} - Mode evaluation : {self.mode == Const.MODE_EVALUATION}')

        ui_modes = {
            'cli': CLIUserInterface,
            'tk': TKUserInterface,
            'evaluation': CLIUserInterface
        }
        if not GameService.check_mode_selection(self.mode):
            self.__log.debug('mauvais mode selectionné, cli sera utilisé par defaut')
        self.user_interface = ui_modes.get(self.mode, 'cli')
        game = self.user_interface(lang=self.language).init_game(mode='evaluation' if self.evaluation else None)
        game.start()
    # def open_cli(self):
    #     self.init_game_cli().start()
    #
    # def init_game_cli(self):
    #     print(f'Welcome to {self.name}')
    #     available_game_list = ['president']
    #     # available_game_list = load_all_csv_rules
    #     game = input(f'quel jeu parmi {available_game_list}? :')
    #     nb_players = int(input('combien de joueur : '))
    #     rules = {
    #         'mode': 'trouduc', 'other': game
    #     }
    #     game = Game(rules=rules)
    #     for player_nb in range(nb_players):
    #         current_player_name = input(f'nom joueur {player_nb} : ')
    #         current_player = HumanPlayer(current_player_name)
    #         game.register_player(current_player)
    #     # game.register_player(AIPlayer())
    #     # game.register_player(AIPlayer('test'))
    #     return game
    #
    # def open_tk(self):
    #     ui = TKUserInterface()
    #     ui.run()
    #
    #
    # def debug(self):
    #     print('debug')

    # def init_lang(self):
    #     self.language_file = 'lang_' + self.language

