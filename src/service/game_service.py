"""
game_service module
"""
import dataclasses
import logging


# class GameService:
#     """
#     GameService class
#     """
#
#     @staticmethod
#     def check_mode_selection(mode):
#         """
#         valid the app display_mode parameter (cli, tk...)
#         :param mode:
#         :return:
#         """
#         return mode in ['cli', 'tk', 'evaluation']


@dataclasses.dataclass
class GameService:
    """
    GameService class
    """
    log: logging

    @staticmethod
    def check_mode_selection(mode):
        """
        valid the app display_mode parameter (cli, tk...)
        :param mode:
        :return:
        """
        return mode in ['cli', 'tk', 'evaluation']

# def bore(worm: Worm):
#     print(f"{worm.name} is boring into {worm.fruit_of_residence}")
#
#
# # or
#
# def bore(fruit: Fruit, worm_name: str):
#     print(f"{worm_name} is boring into {fruit}")
