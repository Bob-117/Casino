"""
debug logger decorator
"""
def logging_decorator(func):
    """
    debug logger # TODO remove
    :param classname:
    :param func:
    :return:
    """

    def wrapper(*args, **kwargs):
        print(f'\n * {func.__name__} ')
        return func(*args, **kwargs)

    return wrapper


def user_error_decorator(func):
    """
    User input error
    :param func:
    :return:
    """
    def wrapper(*args, **kwargs):
        # do something with user service
        pass
        return func(*args, **kwargs)

    return wrapper
