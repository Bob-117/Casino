"""
Module to handle language module import
"""
import importlib

from src.utils import Const


def load_lang_module():
    """
    load a language dictionary module to adapt output
    :return:
    """
    language_module = None
    lang_input = input('lang ? [EN/FR]')  # TODO env
    language_file = Const.LANG_PATH + lang_input.lower()
    try:
        language_module = importlib.import_module(language_file, 'src')
        print(language_module.LOAD_LANG_SUCCESS)
    except ModuleNotFoundError:
        print('cant load lang module, default will be EN')
        language_module = importlib.import_module(Const.LANG_LIST_MODULE[0], 'src')
    finally:
        return language_module
    # if language_file in Const.LANG_LIST_MODULE:
    # return language_module if language_module in Const.LANG_LIST_MODULE else None
