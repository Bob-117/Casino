"""
Const module, later in env var (?)
"""


class Const:
    LANG_LIST = ['FR', 'EN']
    LANG_LIST_MODULE = ['src.utils.lang.lang_en', 'src.utils.lang.lang_fr']
    LANG_PATH = '.utils.lang.lang_'
    DEFAULT_MODE = 'cli'
    MODE_LIST = ['cli', 'tk', 'evaluation']
    MODE_EVALUATION = 'evaluation'
    GAME_LIST = ['president', 'debug']
    AVAILABLE_NB_CARD = [1, 2, 3, 4]
