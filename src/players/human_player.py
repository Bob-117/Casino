"""
Human Player module, inherit from Player
"""
from src.cards import Card
from src.players import Player


class HumanPlayer(Player):
    """
    Human Player class, inherit from Player
    """

    def __init__(self, name):
        # super(HumanPlayer, self).__init__(name)
        super().__init__(name)

    def pick_a_card(self, nb) -> [Card]:
        """
        Human Player select cards he will play (depending on the game rules if exists)
        :return:
        """
        print(f'PICK A CARD PLAYER HUMAN {nb}')
        card = self.hand[self.pick_a_card_entry_cli()]
        self.hand.remove(card)
        return card

    def pick_a_card_entry_cli(self):
        """
        User input to select what card he will play
        :return:
        """
        user_input = input('card ? :')
        while not user_input.isnumeric():
            while int(user_input) > len(self.hand):
                user_input = input('card ? :')
        return int(user_input)

    def __str__(self):
        return f'{self.name} - Human'

    def init_round(self, available_nb_card=None):
        """
        How many cards will u play
        :return:
        """
        # if available_nb_card is None:
        #     available_nb_card = []
        # TODO check if player has a double or a triple card
        # print('--------')
        # self.sort_cards()
        # self.sort_cards_combo()
        # print('--------')
        user_input = input(f'nb card to play ? {available_nb_card}:')

        # TODO player service
        while not user_input.isnumeric() or int(user_input) not in available_nb_card:
            user_input = input(
                f'nb card to play ? {available_nb_card} '
                f'{"int please" if not user_input.isnumeric() else "valid entry please"}:')
        return int(user_input)
        # while not user_input.isnumeric():
        #     user_input = input(f'nb card to play ? {available_nb_card} (int please):')
        #
        #     while int(user_input) not in available_nb_card:
        #         print(f'not in : {int(user_input) not in available_nb_card}')
        #         user_input = input(f'nb card to play ? {available_nb_card}:')
        # return int(user_input)

    # def user_entry_cli(self, msg, condition):
    #     user_input = input(msg)
    #
    #     while not user_input.isnumeric():
    #         while int(user_input) condition:
    #             user_input = input(f'nb card to play ? :')
    #     return int(user_input)
