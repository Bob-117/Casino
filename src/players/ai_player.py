"""
AI Player module, inherit from Player
"""
import numpy as np

from src.cards import Card
from src.players import Player


class AIPlayer(Player):
    """
    AI Player class, inherit from Player
    """

    def __str__(self):
        return f'{self.name} - AI'

    def init_round(self, available_nb_card=None):
        """
        How many cards will u play
        :return:
        """
        if available_nb_card is None:
            available_nb_card = []
        return np.random.choice(available_nb_card)
        # while not user_input.isnumeric():
        #     user_input = input(f'nb card to play ? {available_nb_card} (int please):')
        #
        #     while int(user_input) not in available_nb_card:
        #         print(f'not in : {int(user_input) not in available_nb_card}')
        #         user_input = input(f'nb card to play ? {available_nb_card}:')
        # return int(user_input)

    # def pick_a_card(self) -> [Card]:
    #     ...

    def count_cards(self):
        """
        the AI know all the cards and cards
        we can compile statistics comparing self.hand, game.played_cards & total deck
        :return:
        """
        ...
