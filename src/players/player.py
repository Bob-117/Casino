"""
Player module
abstract, parent/base class of human_player & ai_player
"""
import itertools
from abc import ABC

import numpy as np

from src.cards import Card


class Player(ABC):
    """
    Main abstract Player class with a random name and an empty hand
    """

    __RANDOM_NAME = [
        'Jimi Hendrix', 'Angus Young', 'Joel O\'Keeffe',
        'Keith Richard', 'Jack White', 'Jimmy Page'
    ]

    def __init__(self, name=None):
        """
        Player class constructor with a random name and an empty hand
        """
        self.__name = name or np.random.choice(self.__RANDOM_NAME)
        self.__hand = []
        self.fold = False

    @property
    def name(self):
        """
        Name attribute getter
        """
        return self.__name

    @name.setter
    def name(self, new_name):
        """
        Name attribute setter
        :param new_name: Str
        """
        self.__name = new_name

    def __str__(self):
        return f'{self.name} - Player'

    @property
    def hand(self):
        """
        Hand attribute getter
        """
        return self.__hand

    @hand.setter
    def hand(self, new_hand):
        """
        Hand attribute setter
        :param new_hand: [Card]
        """
        self.__hand = new_hand

    # def play(self):
    #     """
    #     Player play one or many cards (depending on the game rules if exists)
    #     :return: Card
    #     """
    #     return self.pick_a_card()

    def pick_a_card(self, nb) -> [Card]:
        """
        PLayer select cards he will play (depending on the game rules if exists)
        :return:
        """
        # return np.random.choice(self.__hand)
        print(f'PICK A CARD PLAYER BASE {nb}')
        picked_card = self.__hand[0]

        self.__hand.remove(self.__hand[0])
        return picked_card

    def pick_a_card_entry(self):
        """
        pick a card with user cli input
        :return:
        """
        user_input = input('card ? :')
        # FIXME player service
        while not user_input.isnumeric():
            while int(user_input) > len(self.hand):
                user_input = input('card ? :')
        return user_input

    def first_player_special_card(self, special_card: Card) -> bool:
        """

        :param special_card: Card
        :return:
        """
        # print('--------1st p special card--------')
        # print(special_card)
        # print('----------------')
        return special_card.im_in_hand(self.hand)
        # return self.__hand == [] or True

    def init_round(self, available_nb_card=None):
        """
        How many cards will u play
        :return:
        """

    def sort_cards(self):
        self.hand.sort()

    def sort_cards_combo(self):
        self.sort_cards()
        # combo = {
        #     1: [],
        #     2: [],
        #     3: [],
        #     4: []
        # }
        #
        # for card in self.hand:
        #     combo.get(1).append(card)
        # newlist = [
        #     [
        #         card_in_hand for card_in_hand in self.hand
        #         if (card_in_hand.face != current_card.face and card_in_hand.value == current_card.value)
        #      ]
        #     for current_card in self.hand
        # ]
        # print(newlist)
        combo = [(card.value, list(group)) for card, group in itertools.groupby(self.hand)]
        return combo


    # def at_least_one_card_to_play(self, top_card):
    #     """
    #     last card is K, I can only play A & 2
    #     :param top_card:
    #     :return:
    #     """
    #     print(lambda: any(card in self.hand and card > card for card in top_card))
    #     return lambda: any(card in self.hand and card > card for card in top_card)

    def __repr__(self):
        return f'{self.name} - {self.hand}'
