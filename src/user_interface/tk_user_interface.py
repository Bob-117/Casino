"""
TKInter User Interface module, inherit from UserInterface
"""
import tkinter as tk
from tkinter import ttk as HAHA

from src.user_interface import UIInterface
from src.players import HumanPlayer
from src.gambling_house.game import Game
from src.utils import logging_decorator


class TKUserInterface(UIInterface):
    """
    TKInter User Interface class, inherit from UserInterface
    https://www.tutorialspoint.com/how-to-change-entry-get-into-an-integer-in-tkinter
    https://www.pythontutorial.net/tkinter/tkinter-listbox/
    """

    def __init__(self, lang):
        super().__init__(lang=lang)
        print('tk init')
        self.window = tk.Tk()
        self.game = None

    def init_game(self, mode=None):
        """
        create game in tk with user input
        :return:
        """
        # print(f'Welcome to {self.name}')
        self.window.title(f'Casino {"evaluation" if mode == "evaluation" else ""}')

        self.window.geometry("700x350")

        self.window.grid_rowconfigure(0, weight=0)
        self.window.grid_columnconfigure(0, weight=1)

        self.select_a_game()

        # frame = tk.Frame(self.window)
        # frame.grid(row=0, column=0, sticky="nsew")
        #  ####
        # self.create_a_game()
        self.window.mainloop()

    @logging_decorator
    def start_a_game(self, nb_players):
        """
        X input text for players name => send to register_named_players()
        :param nb_players:
        :return:
        """
        self.clear_frame()
        player_names_input = []
        for player_counter in range(nb_players):
            tmp_player_name_input = tk.Entry(self.window, width=35, name=str(player_counter))
            player_names_input.append(tmp_player_name_input)
            tmp_player_name_input.grid(row=player_counter, column=0)

        register_button = tk.Button(master=self.window, text='register',
                                    command=lambda: self.register_named_players(player_names_input)
                                    )
        register_button.grid(column=0)

    def register_named_players(self, names_array):
        """
        register new players, generate & deal cards => Round ???? # TODO
        :param names_array:
        :return:
        """
        # map tk_entry to string name list
        self.game.register_players(
            list(
                map(
                    lambda player_entry: HumanPlayer(name=player_entry.get()), names_array)
            )
        )
        # TODO register ai players
        self.game.generate_cards()
        self.game.deal_cards()
        self.deal_cards()

    def select_a_game(self):
        """
        ComboBox (list option) to pick a game => send to create_a_game()
        :return:
        """
        game_list = ["Option1", "Option2", "Option3", "Option4", "Option5"]
        game_list_choice = HAHA.Combobox(self.window, values=game_list)
        game_list_choice.set(f'{self.lang.WHAT_GAME}')
        game_list_choice.grid()
        go_button = tk.Button(master=self.window, text='go', command=self.create_a_game)
        go_button.grid(row=3, column=0)
        # TODO get all games register in rules.csv
        # langs = ('Java', 'C#', 'C', 'C++', 'Python',
        #          'Go', 'JavaScript', 'PHP', 'Swift')
        #
        # var = tk.Variable(value=langs)
        #
        # listbox = tk.Listbox(
        #     self.window,
        #     listvariable=var,
        #     height=4,
        #     # selectmode=tk.EXTENDED
        # )
        #
        # listbox.pack(expand=True, fill=tk.BOTH)

    def create_a_game(self):
        """
        create a game & ask for nb of players, => send to go()
        :return:
        """
        self.clear_frame()
        label = tk.Label(self.window, text="Enter number player", font=('Calibri 10'))
        label.grid(row=0, column=0)
        nb_player_entry = tk.Entry(self.window, width=35)
        nb_player_entry.grid(row=2, column=0)
        game = 'TK INPUT PRESIDENT'
        rules = {
            'mode': 'trouduc', 'other': game
        }
        self.game = Game(rules=rules)

        go_button = tk.Button(
            master=self.window, text='go',
            command=lambda: self.start_a_game(
                int(nb_player_entry.get())
            )
        )
        go_button.grid(row=3, column=0)
        # label.grid_rowconfigure(1, weight=1)
        # label.grid_columnconfigure(1, weight=1)

    def deal_cards(self):
        """
        deal cards
        :return:
        """
        print('deal card tk')
        print(self.game.players)
        self.clear_frame()
        for i, _ in enumerate(self.game.players):
            for j, card in enumerate(_.hand):
                label = tk.Label(text=f'{i}-{j}-{card}')
                label.grid(row=i, column=j)
                # label = tkinter.Label(textvariable=var)

    def clear_frame(self):
        """
        clear all widgets from main windows # TODO user tk.Frame()
        :return:
        """
        for widgets in self.window.winfo_children():
            widgets.destroy()
