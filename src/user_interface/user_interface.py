"""
user interface abstract class
"""
from abc import ABC


class UIInterface(ABC):
    """
    user interface abstract class
    """

    def __init__(self, lang):
        self.lang = lang

    def init_game(self):
        """
        ask nb player & what game
        :return:
        """

    def start_game(self):
        """
        start game with given rules, nb player
        :return:
        """
