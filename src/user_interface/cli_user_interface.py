"""
CLI User Interface module, inherit from UserInterface
"""
from src.user_interface import UIInterface
from src.utils import logging_decorator, Const
from src.players import HumanPlayer, AIPlayer
from src.gambling_house.game import Game


class CLIUserInterface(UIInterface):
    """
    CLI User Interface class, inherit from UserInterface
    """
    def __init__(self, lang):
        super().__init__(lang)
        self.name = 'GIBSON'

    @logging_decorator
    # @decorator_fun(mode='cli')
    def init_game(self, mode=None):
        """
        Game initialisation
        if mode == evaluation:
            default game is president, with 1 human player & 3 AI players
        :param mode:
        :return:
        """
        if mode == 'evaluation':
            game_name = self.init_game_evaluation()
            print('mode evaluation (3 ai + 1 human), creating game president')
            rules = {
                'other': game_name
            }
            game = Game(rules=rules)
            player_name = (input('votre nom : '))
            game.register_players([AIPlayer(), AIPlayer(), AIPlayer(), HumanPlayer(player_name)])
        else:
            game_name = self.init_game_default()
            print(f'mode normal, creating game {game_name}')
            rules = {
                'other': game_name, 'mode': 'trouduc'
            }
            game = Game(rules=rules)
            nb_players = int(input('combien de joueur : '))
            rules = {
                'other': game, 'mode': 'trouduc'
            }
            game = Game(rules=rules)
            for player_nb in range(nb_players):
                current_player_name = input(f'nom joueur {player_nb} : ')
                current_player = HumanPlayer(current_player_name)
                game.register_player(current_player)

        return game

    @logging_decorator
    def start_game(self):
        """
        init a new game (what game, players, rules)
        & start it
        :return:
        """
        self.init_game().start()

    def init_game_default(self):
        """
        default game initialisation, user pick a game
        :return:
        """
        game = self.pick_game_choice()
        return game


    def init_game_evaluation(self):
        """
        init a 'President' game for evaluation
        :return:
        """
        return 'president'

    def pick_game_choice(self):

        game_list = Const.GAME_LIST
        # game_menu = ''.join(enumerate(Const.GAME_LIST))
        game_menu = "\n".join(f"{key + 1}: {value}" for key, value in enumerate(game_list))
        choice = input(f'{self.lang.WHAT_GAME} :\n{game_menu} \n =>')
        game_name = None
        # Init ou 2 conditions
        string_condition = choice in game_list
        int_condition = choice.isnumeric() and 0 <= int(choice) <= len(game_list)
        if string_condition:
            game_name = choice
        elif int_condition:
            game_name = game_list[int(choice)]
        else:
            while not string_condition or int_condition:
                game_menu = "\n".join(f"{key + 1}: {value}" for key, value in enumerate(game_list))
                choice = input(f'quel jeu parmi (reponse valable only)? :\n{game_menu} \n =>')
                string_condition = choice in game_list
                int_condition = choice.isnumeric() and 0 <= int(choice) <= len(game_list)
                game_name = game_list[int(choice)] if (
                        choice.isnumeric()
                        and 0 <= int(choice) <= len(game_list)
                ) else (choice if choice in game_list else 'nope')
                # if choice.isnumeric() and 0 <= int(choice) < len(game_list):
                #     print('int cond')
                #     game_name = game_list[int(choice)]
                # elif choice in game_list:
                #     print('str cond')
                #     game_name = choice
                # else:
                #     print('nope')
                #     game_name = 'nope'
                if string_condition:
                    game_name = choice
                elif int_condition:
                    game_name = game_list[int(choice)]
                else:
                    print('nope')
                # game_name = choice if choice in game_list else ()
                # print(f'in while - {int_condition} - {string_condition}')
        return game_name

        # game_list = Const.GAME_LIST
        # # game_menu = ''.join(enumerate(Const.GAME_LIST))
        # game_menu = "\n".join(f"{key + 1}: {value}" for key, value in enumerate(game_list))
        # choice = input(f'{self.lang.WHAT_GAME} :\n{game_menu} \n=>')
        # game_name = None
        # # Init ou 2 conditions
        # string_condition = choice in game_list
        # int_condition = choice.isnumeric() and (1 <= int(choice) <= len(game_list) + 1)
        #
        # print(f'{int_condition} - {string_condition} - {choice} - {len(game_list)}')
        #
        # while not (string_condition or int_condition):
        #     game_menu = "\n".join(f"{key + 1}: {value}" for key, value in enumerate(game_list))
        #
        #     choice = input(f'quel jeu parmi (reponse valable only)? :\n{game_menu} \n=>')
        #     string_condition = choice in game_list
        #     int_condition = choice.isnumeric() and 1 <= int(choice) <= len(game_list) + 1
        #     if int_condition and not string_condition:
        #         game_name = game_list[int(choice) - 1]
        #     if string_condition and not int_condition:
        #         game_name = choice
        #
        #     print(game_name)
        #     return 'president'


