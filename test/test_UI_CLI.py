import builtins
from unittest import mock

from src.utils import load_lang_module
from src.user_interface import CLIUserInterface


def test_init_cli_ui():
    with mock.patch.object(builtins, 'input', lambda _: 'EN'):
        lang_module = load_lang_module()
        cli_ui_test = CLIUserInterface(lang_module)
        assert isinstance(cli_ui_test, CLIUserInterface)
