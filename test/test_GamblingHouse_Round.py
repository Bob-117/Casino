from src.players import Player
from src.cards import Card
from src.gambling_house import Round

def test_card_in_hands():
    players = [Player(), Player()]
    round_test_void_2p = Round(options={'special_card': Card('Q', '♡')}, players=players)
    assert round_test_void_2p.card_in_hands() == [False, False]

    players.append(Player())
    round_test_void_3p = Round(options={'special_card': Card('Q', '♡')}, players=players)
    assert round_test_void_3p.card_in_hands() == [False, False, False]

    players[2].hand = [Card('Q', '♡')]
    round_test = Round(options={'special_card': Card('Q', '♡')}, players=players)
    assert round_test.card_in_hands() == [False, False, True]

    players[0].hand = [Card('K', '♡'), Card('K', '♡')]
    players[1].hand = [Card('Q', '♡')]
    players[2].hand = []
    round_test = Round(options={'special_card': Card('Q', '♡')}, players=players)
    assert round_test.card_in_hands() == [True, True, False]


def test_card_in_hands_2p_empty():
    players = [Player(), Player()]
    round_test_void_2p = Round(options={'special_card': Card('Q', '♡')}, players=players)
    assert round_test_void_2p.card_in_hands() == [False, False]


def test_card_in_hands_3p_empty():
    players = [Player(), Player(), Player()]
    round_test_void_3p = Round(options={'special_card': Card('Q', '♡')}, players=players)
    assert round_test_void_3p.card_in_hands() == [False, False, False]


def test_select_first_player():
    player1, player2 = Player(), Player()
    player2.hand = [Card('Q', '♡')]
    round_init_test = Round(options={'special_card': Card('Q', '♡')}, players=[player1, player2])
    round_init_test.select_first_player()
    assert round_init_test.first_player == player2
    round_init_test.winner = player1
    round_play_test = Round(options={}, players=[player1, player2], last_round=round_init_test)
    round_play_test.select_first_player()
    assert round_play_test.first_player == player1



