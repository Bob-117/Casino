import builtins
from unittest import mock

from src.cards import Card
from src.players import Player


def test_init():
    player = Player()
    assert player.name != ''
    assert player.hand == []


def test_pick_a_card_empty_hand():
    # assert error
    pass


def test_pick_a_card_one():
    player = Player(name='bob')
    player.hand = ([Card(face='♡', value='1')])
    with mock.patch.object(builtins, 'input', lambda _: '0'):
        assert player.pick_a_card(nb=1) == Card(face='♡', value='1')
    pass


def test_pick_a_card_many():
    player = Player(name='bob')
    player.hand = ([Card(face='♡', value='1'), Card(face='♡', value='2'), Card(face='♡', value='3')])
    # assert isinstance(Card, player.pick_a_card())
    with mock.patch.object(builtins, 'input', lambda _: '0'):
        # assert player.pick_a_card() in player.hand
        assert player.pick_a_card(nb=1) == Card(face='♡', value='1')
        # assert player.hand
        # assert player.hand == [Card(face='♡', value='1'), Card(face='♡', value='2')]
    pass


def test_first_player():
    player = Player(name='bob')
    player.hand = ([Card(face='♡', value='1')])
    player.hand = ([Card(face='♡', value='10')])
    # assert player1 is first player
    pass
