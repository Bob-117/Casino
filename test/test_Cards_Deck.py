from src.cards import Deck


def test_init_deck():
    deck_test = Deck()
    assert isinstance(deck_test, Deck)
    assert deck_test.cards == []

