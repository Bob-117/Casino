import numpy as np


class Player:
    def __init__(self, name=None):
        self.__RANDOM_NAME = [
            'Jimi Hendrix', 'Angus Young', 'Joel O\'Keeffe',
            'Keith Richard', 'Jack White', 'Jimmy Page'
        ]
        self.__name = name or np.random.choice(self.__RANDOM_NAME)
        self.__hand = []

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        self.__name = new_name

    @property
    def hand(self):
        return self.__hand

    @hand.setter
    def hand(self, new_hand):
        self.__hand = new_hand

    def add_to_hand(self, cards):
        """
        add one or many card to a player hand
        :param cards:
        :return:
        """
        self.hand.append(cards)
        # if isinstance(cards, list):
        # if isinstance(cards, Card):

    def remove_from_hand(self, card):
        """
        remove one or many cards from hand
        :param card:
        :return:
        """
        self.hand.remove(card)

    def play(self):
        """
        play one or many cards
        AI will play randomly
        Human will play with cli input for now
        :return:
        """
        pass


class AIPlayer(Player):
    def __init__(self, name=None):
        super().__init__(name)
        self.__name = name or 'Trump'

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        self.__name = new_name

    def play(self):
        # TODO
        return np.random.choice(self.hand)


class HumanPlayer(Player):
    def __init__(self):
        super().__init__()
        # self.name = input('Votre nom :')

    def play(self):
        """
        Play a card asking cli input
        :return:
        """
        # TODO  playing one or many cards ?
        # TODO check only playable cards depending on the game rules
        return self.hand[(int(input('Card ? :')))]
