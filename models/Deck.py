from random import shuffle
from models import Card

import numpy as np


class Deck:
    def __init__(self):
        self.__cards: [Card] = self.create_cards()

    @property
    def cards(self):
        return self.__cards

    def create_cards(self):
        """
        create a standard 52 cards deck
        :return:
        """
        faces = ['♡', '♣', '♢', '♤']
        values = [3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", 1, 2]
        temp_cards = []
        for value in values:
            for face in faces:
                card = Card(value, face)
                temp_cards.append(card)

        # for value, face in zip(values, faces):
        #     card = Card(value, face)
        #     temp_cards.append(card)

        # a = np.array(
        #     [
        #         [3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", 1, 2],
        #         ['♡', '♣', '♢', '♤'],
        #     ],
        #     dtype=object
        # )
        # print(np.repeat(a[1], a[0]))

        return temp_cards

    def shuffle(self):
        shuffle(self.cards)  # TODO care about random.seed
