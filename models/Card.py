import numpy as np


class Card:
    def __init__(self, value, face):
        self.value = value
        self.face = face

    def value_to_power(self):  # TODO utils / card service
        """
        Associate card value to real power in game, depending on game rules
        Use a np array as rule documentation => use csv later ?
        :return:
        """
        values = np.array([
            [
                [3, '3'],
                [4, '4'],
                [5, '5'],
                [6, '6'],
                [7, '7'],
                [8, '8'],
                [9, '9'],
                [10, '10'],
                ['J', 'V', 'Valet'],
                ['Q', 'D', 'Dame'],
                ['K', 'R', 'Roi'],
                ['A', 1, 'As'],
                [2, '2']
            ],
            [
                [1],
                [2],
                [3],
                [4],
                [5],
                [6],
                [7],
                [8],
                [9],
                [10],
                [11],
                [12],
                [13]
            ]
        ], dtype=object)
        for _ in range(values.shape[-1]):  # TODO np.nditer
            true_value, true_power = values[0, _], values[1, _]
            if self.value in true_value:
                return true_power[0]
        return -1  # TODO ur a cheater, ur custom card wont win u the game

    def __gt__(self, other):
        return self.value_to_power() > other.value_to_power()

    def __lt__(self, other):
        return self.value_to_power() < other.value_to_power()

    def __eq__(self, other):
        return self.value_to_power() == other.value_to_power()

    def __str__(self):
        return f'{self.value}-{self.face}'

    def __repr__(self):
        return f'{self.value}-{self.face}'
