import logging
from random import shuffle

from models import HumanPlayer, AIPlayer, Deck, Player, Card


class PresidentGame:
    """
    President Game
    3 players - 52 cards
    faces = ['♡', '♣', '♢', '♤']
    values = [3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", 1, 2]
    """
    def __init__(self):
        self.__log = self.init_logger()
        self.players: [Player] = None
        self.deck: Deck = Deck()
        self.unused_cards: [Card] = []
        self.start()

    def init_logger(self):
        logger = logging.getLogger(self.__class__.__name__)
        FORMAT = '%(asctime)s %(levelname)-8s %(name)-15s %(message)s'
        DATEFMT = '%Y-%m-%d %H:%M:%S'
        logging.basicConfig(format=FORMAT, datefmt=DATEFMT, level='INFO')  # TODO env
        return logger

    def create_players(self) -> None:
        """
        Create players for president game
        For evaluation & test, we assert we have 1 Human_Player and 2 AI_Player
        :return:
        """
        self.__log.debug('Creating players')
        self.players = [HumanPlayer(), AIPlayer(), AIPlayer()]
        shuffle(self.players)

    def distribute_cards(self) -> None:
        """
        split current deck in equal parts between all players and fill their hands
        keep unused_cards
        :return:
        """
        self.__log.debug('Dealing cards')
        hands = self.split_cards_between_players()
        for i, current_hand in enumerate(hands):
            current_player = self.players[i]
            # print(f'{current_player.name} == {len(current_hand)} == {current_hand}')
            current_player.hand = current_hand

    def split_cards_between_players(self) -> [[Card]]:
        """
        split current deck in equal parts between all players
        keep unused_cards
        :return:
        """
        self.__log.debug('Splitting cards')
        # Make sure all players have same card number in hand
        unused_cards_nb = len(self.deck.cards) % len(self.players)
        used_cards_nb = len(self.deck.cards) // len(self.players)
        self.deck.shuffle()
        # By removing unsused cards before dealing deck
        for _ in range(unused_cards_nb):
            self.unused_cards.append(self.deck.cards.pop())
        # self.used_cards = self.deck
        return [self.deck.cards[i::len(self.players)] for i in range(0, len(self.players))]

    def start(self):
        self.__log.debug('Starting game')
        self.create_players()
        self.distribute_cards()
