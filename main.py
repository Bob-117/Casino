from src.utils import load_lang_module
from src import Casino

if __name__ == '__main__':
    # print(lang.WELCOME)
    main_language = load_lang_module()
    mode = input('mode ? [cli/tk/evaluation]')  # TODO env
    casino = Casino(mode=mode, lang=main_language)
    casino.open()
