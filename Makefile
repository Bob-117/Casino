run:
	python3 main.py

exercice1:
	python3 -m unittest test/Evaluation/test_exercice1.py

exercice2:
	python3 -m unittest test/Evaluation/test_exercice2.py

test_player:
	python3 -m pytest test/test_Player.py

test_eval: exercice1 exercice2

test_custom:
	python3 -m pytest test/
test_all: test_eval test_custom