## EPSI B3 PYTHON 2022 / 2023

### Jeu du president
### POO Python

## https://gitlab.com/docusland-courses/python/python-president-game

```shell
git clone git@gitlab.com:Bob-117/Casino.git
cd Casino # all good code will be on branch develop, maybe not tagged on master
pip install -r requirements.txt
python3 main.py
```
_note : le choix de la langue (en ou fr) et le mode d'affichage (cli, evaluation, tk) seront passés en paramètres docker. Le mode evaluation lance une game président_

[[_TOC_]]

La mise en place d'une pipeline ci/cd a été mise en place sur ce projet ce qui garantit que tous les tests sont executés.
L'implémentation du code a suivi une logique CDD / TDD.


Les classes & les tests associés sont respectivement dans */models* et dans */test/Evaluation/*.
Les fichiers de test *test_exercice1.py* et *test_exercice2.py* sont identiques à ceux fournits.
Les classes utilisées pour l'evaluation (dans *models*) peuvent donc differer de celles dans *src*.


<details>
<summary>Execution des tests :</summary>

- [ ] Tests requis (unitest):
```shell
    make exercice1
    make exercice2
    make test_eval
```
    
- [ ] Tests custom (pytest, concernant les classes dans */src*):
```shell
    make test_player
    make test_custom
```

- [ ] Tous les tests :
```shell
    make test_all
```


</details>

<details>
<summary> Exercice 1</summary>

Générer un deck de 52 cartes.
Rédiger les méthodes magiques permettant de comparer deux cartes.
Attention, la carte la plus forte est le 2, puis l'as, puis le R, D, V, 10, 9...
.
Afin de vous assurer que le code généré fonctionne. Executez
les tests suivants.
```shell
# commande requise
python test_exercice1.py
# commande mise en place
python3 -m unittest test/Evaluation/test_exercice1.py
make exercice1
```
</details>

<details>
<summary> Exercice 2 </summary>

Afin de pouvoir jouer au président, il va être nécessaire d'implémenter la
classe d'un joueur. Et de distribuer les cartes aux joueurs.
Afin de faciliter les tests, nous allons considérer qu'il y a trois joueurs
présents autour de la table.
Ainsi, dans cette étape, implémentez les classes PresidentGame et Player.
```shell
# commande requise
python test_exercice2.py
# commande mise en place
python3 -m unittest test/Evaluation/test_exercice2.py
make exercice2
```

</details>

<details>
<summary> Exercice 3</summary>

## todo :
- [ ] Ce jeu se joue de 3 à 6 joueurs.
  - [X] 14/11/22 3 joueurs requies
- [ ] Lors du premier tour, le joueur possédant la dame de coeur commence.
- [X] L'ensemble des cartes sont distribuées aux joueurs de la manière la plus homogène.
```python
# see models/PresidentGame.py
from models import PresidentGame
game = PresidentGame()
game.split_cards_between_players()
```
- [ ] Ce jeu se joue par tours. Tant que quelqu'un peut et veut jouer, le tour continue et tourne dans le sens horaire.

J'ai implémenté une classe Round qui fonctionne de facon recursive.
```python
from src.gambling_house import Round
_round = Round(last_round=None).run
while ('condition'):
    _round= Round(last_round=_round)
```
utilisée dans la classe game :

```python
# see src/gambling_house/round.py
from src.gambling_house import Round
class Game:
    def __init__(self, players, cards):
        self.players = players
        self.folded_players = []
    def start(self):
        _round = Round(players=self.players, options={'state': 'init'}, last_round=None).run()
        while len(self.folded_players) != len(self.players) - 1:
            _round = Round(players=self.players, options={'state': 'init'}, last_round=_round).run()
```

- [ ] Le premier joueur choisit des cartes d'une même valeur et les pose sur la tables.
```python
class Round:
    
    def __init__(self, state, players, last_round, options):
        self.state = state
        self.players = players
        self.first_player = None
        self.last_round = last_round
        self.options = options
        
    def run(self):
        self.select_first_player()
        # logic
    
    def select_first_player(self):
        # au premier tour, le premier joueur est celui qui a la carte spéciale
        if self.state == 'init':
            for player in self.players:
                if player.first_player_special_card(self.options['special_card']):
                  self.first_player = player
                  print(f'{player.name} will start, hes has {self.options["special_card"]}')
        # pour les tours suivants, le premier joueur est le gagnant/perdant du tour précent (dependant des règles du jeu en cours)
        elif self.state == 'play': 
            self.first_player = self.last_round.winners[0]
            print(f'{self.first_player.name} will start, hes won last round')

```
- [ ] Suite à celà, chaque joueur doit fournir autant de cartes que le joueur précédent des cartes dun' valeur supérieure ou égale.
```python
import itertools

class Player:
    def sort_cards(self):
      self.hand.sort()
    def sort_cards_combo(self):
        self.sort_cards()
        combo = [(card.value, list(group)) for card, group in itertools.groupby(self.hand)]
        return combo
```
- [ ] Un joueur a le droit de sauter son tour et reprendre le tour d'après.
- [ ] Un tour est fini lorsque plus personne ne joue. C'est alors le dernier à avoir joué qui ouvre la manche suivante. Ou si un joueur pose un ou plusieurs deux. Ce qui mets immédiatement fin au tour.
- [ ] L'objectif est d'être le premier à ne plus avoir de cartes. Ce joueur est alors déclaré président de la manche.
- [ ] Les joueurs restants continuent à jouer jusqu'à ce qu'il n'y ait plus qu'une joueur qui ait des cartes en main, il est alors déclaré 'troufion'


```
pylint src
Your code has been rated at 9.19/10 (previous run: 8.79/10, +0.40)
```
</details>


<details>
<summary> Implémentation des classes </summary>

```mermaid
flowchart TB
    Casino["CASINO"]
    open["open()"]
    cli["CLI UI"]
    tk["TK UI"]
    ui["UI"]
    Game["GAME"]
    Game1["start()"]
    Round["Round class (recursive)"]
    
    Casino-- display_mode & \n language selection -->open
    open-- select a game\n register players --> cli & tk --> ui
    ui -- init game and register players --> Game
    Game --> Game1
    Game1 --> Round -- loop while len winnners != len players -1 --> Round
    Round -- end game --> UI

```
</details>

